/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painter;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import javax.swing.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import painter.widgets.ClipboardImage;

/**
 *
 * @author yen
 */
public class PainterCanvas extends JPanel {
    public enum Functions {
        LINE, 
        RECTANGLE_SELECTION, 
        MOVING_RECTANGLE, 
        RECTANGLE, 
        ELLIPSE, 
        POLYGON, 
        ROUND_RECTANGLE, 
        PENCIL, 
        FLOOD, 
        ERASER, 
        PICK_COLOR, 
        SPRAYER, 
        TEXT, 
        CURVE, 
        CURVE_SELECTION, 
        MOVING_ARBITRARY
    };
    public enum MouseButton {
        LEFT, 
        RIGHT, 
        NONE
    }
    public enum FillType {
        BORDER, 
        BOTH, 
        INTERNAL
    }
    public final static int[] defaultSize = { 512, 384 };
    private final ArrayList<BufferedImage> bufferedImages;
    private int imageIndex;
    private MouseButton mouseButton;
    private boolean saved;
    private boolean hasSelection;
    private Rectangle selectionRect;
    private ArrayList<Point> selectionCurve;
    private Polygon selectionPolygon;
    private Point startPoint;
    private Functions function;
    private FillType fillType;
    private Color leftColor;
    private Color rightColor;
    private final ArrayList<Point> polygonPoints;
    private int penWidth;
    private final Stroke dottedLine;
    private final ArrayList<PainterCanvasEventListener> painterCanvasEventListeners;
    private Timer sprayerTimer;
    private Point sprayerPoint;
    private Frame parent;

    public PainterCanvas()
    {
        super();
        function = Functions.LINE;
        leftColor = Color.BLACK;
        rightColor = Color.WHITE;
        penWidth = 1;
        bufferedImages = new ArrayList<>();
        imageIndex = 0;
        saved = true;
        hasSelection = false;
        selectionRect = new Rectangle();
        selectionCurve = null;
        mouseButton = MouseButton.NONE;
        dottedLine = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10, new float[] {10}, 0);
        fillType = FillType.BORDER;
        polygonPoints = new ArrayList<>();
        painterCanvasEventListeners = new ArrayList<>();
        parent = null;
        this.addComponentListener(new CanvasComponentListener());
        this.setSize(defaultSize[0], defaultSize[1]);
        setImage(null);
    }
    
    public void setParent(Frame c)
    {
        parent = c;
    }

    public void setStartPoint(Point pt, MouseButton button)
    {
        startPoint = pt;
        mouseButton = button;
        if(function == Functions.PICK_COLOR)
        {
            Color pickedColor = new Color(getLastBufferedImage().getRGB(pt.x, pt.y));
            switch(mouseButton)
            {
                case LEFT:
                    leftColor = pickedColor;
                    break;
                case RIGHT:
                    rightColor = pickedColor;
                    break;
                default:
                    throw new RuntimeException("Unexpected mouse button");
            }
            firePainterCanvasEvent(new PainterCanvasEvent(mouseButton, pickedColor));
            return;
        }
        if(hasSelection)
        {
            tryRemoveSelection();
            if(selectionCurve == null && selectionRect.contains(pt))
            {
                function = Functions.MOVING_RECTANGLE;
            }
            else if(selectionCurve != null && selectionPolygon.contains(pt))
            {
                function = Functions.MOVING_ARBITRARY;
            }
        }
        if(function == Functions.POLYGON && polygonPoints.isEmpty())
        {
            polygonPoints.add(pt);
        }
        if(function == Functions.PENCIL || function == Functions.ERASER || function == Functions.CURVE_SELECTION)
        {
            polygonPoints.add(pt);
        }
        if(function == Functions.CURVE)
        {
            switch(polygonPoints.size())
            {
                case 0:
                case 1:
                    polygonPoints.add(pt);
                    break;
                case 2:
                    polygonPoints.add(1, pt);
                    break;
                case 3:
                    polygonPoints.add(2, pt);
                    break;
            }
        }
        switch(function)
        {
            case RECTANGLE_SELECTION:
            case CURVE_SELECTION:
                setHasSelection(true);
                break;
            default:
                clearBufferedImagesTail();
                break;
        }
        if(polygonPoints.size() <= 1) // a point may be added above
        {
            addBufferedImage();
        }
        switch(function)
        {
            case FLOOD:
                flood(pt);
                break;
            case SPRAYER:
                sprayerPoint = pt;
                sprayerTimer = new Timer("Sprayer Timer");
                sprayerTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run()
                    {
                        spraying();
                    }
                }, 0, 10);
                break;
            case TEXT:
                TextInputDialog dialog = new TextInputDialog(parent, true);
                dialog.setVisible(true);
                if(!dialog.isCancelled())
                {
                    Graphics2D g = getLastBufferedImage().createGraphics();
                    Font font = dialog.getSelectedFont();
                    g.setFont(font);
                    g.setColor(getMouseColors()[0]);
                    String[] texts = dialog.getText().split("\n");
                    for(int i = 0; i < texts.length; i++)
                    {
                        int x = startPoint.x;
                        int y = startPoint.y + g.getFontMetrics().getHeight() * (i + 1);
                        g.drawString(texts[i], x, y);
                    }
                    g.dispose();
                    this.repaint();
                    saved = false;
                }
                break;
            case CURVE:
                drawCurve(null);
                break;
        }
    }

    public void updatePoint(Point pt)
    {
        if(function == Functions.FLOOD || function == Functions.PICK_COLOR)
        {
            return;
        }

        if(function != Functions.SPRAYER)
        {
            copyFromLastBufferedImage();
        }
        Graphics2D g = getLastBufferedImage().createGraphics();
        Color[] mouseColors = getMouseColors();
        Color basicColor = mouseColors[0];
        Color alternativeColor = mouseColors[1]; // for filling
        g.setColor(basicColor);
        g.setStroke(new BasicStroke(penWidth));

        int x1, y1, x2, y2, left, top, width, height;
        x1 = Util.getPointX(startPoint);
        y1 = Util.getPointY(startPoint);
        x2 = Util.getPointX(pt);
        y2 = Util.getPointY(pt);
        left = Math.min(x1, x2);
        top = Math.min(y1, y2);
        width = Math.abs(x2 - x1);
        height = Math.abs(y2 - y1);

        Shape shape = null;
        switch(function)
        {
            case LINE:
                g.drawLine(x1, y1, x2, y2);
                break;
            case RECTANGLE_SELECTION:
                g.setColor(Color.BLACK);
                g.setStroke(dottedLine);
                g.drawRect(left, top, width, height);
                selectionRect.setLocation(left, top);
                selectionRect.setSize(width, height);
                break;
            case MOVING_RECTANGLE:
                int selectionX = selectionRect.x;
                int selectionY = selectionRect.y;
                int selectionWidth = selectionRect.width;
                int selectionHeight = selectionRect.height;
                g.setColor(Color.WHITE);
                g.fillRect(selectionX, selectionY, selectionWidth, selectionHeight);
                g.drawImage(getImageSelection(), selectionX + (x2 - x1), selectionY + (y2 - y1), selectionWidth, selectionHeight, null);
                break;
            case RECTANGLE:
                if(shape == null)
                {
                    shape = new Rectangle2D.Float(left, top, width, height);
                }
            case ELLIPSE:
                if(shape == null)
                {
                    shape = new Arc2D.Float(left, top, width, height, 0, 360, Arc2D.CHORD);
                }
            case ROUND_RECTANGLE:
                if(shape == null)
                {
                    shape = new RoundRectangle2D.Float(left, top, width, height, 10, 10);
                }
                switch(fillType)
                {
                    case BORDER:
                        g.draw(shape);
                        break;
                    case INTERNAL:
                        g.fill(shape);
                        break;
                    case BOTH:
                        g.setColor(alternativeColor);
                        g.fill(shape);
                        g.setColor(basicColor);
                        g.draw(shape);
                        break;
                }
                break;
            case POLYGON:
            case PENCIL:
            case ERASER:
            case CURVE_SELECTION:
                polygonPoints.add(pt);
                if(function == Functions.ERASER || function == Functions.CURVE_SELECTION)
                {
                    g.setColor(Color.WHITE);
                }
                if(function == Functions.CURVE_SELECTION)
                {
                    g.setStroke(dottedLine);
                }
                g.drawPolyline(Util.getXArray(polygonPoints), Util.getYArray(polygonPoints), polygonPoints.size());
                break;
            case SPRAYER:
                sprayerPoint = pt; // drawing tasks are done in a timer
                break;
            case CURVE:
                switch(polygonPoints.size())
                {
                    case 1:
                        polygonPoints.add(pt);
                        break;
                    case 2:
                    case 3:
                        polygonPoints.remove(1);
                        polygonPoints.add(1, pt);
                        break;
                    case 4:
                        polygonPoints.remove(2);
                        polygonPoints.add(2, pt);
                        break;
                }
                drawCurve(g);
                break;
            case MOVING_ARBITRARY:
                g.setColor(Color.WHITE);
                BufferedImage maskedSelection = getMaskedCurveSelection();
                g.fillPolygon(selectionPolygon); // this should be after mask calculation
                g.drawImage(maskedSelection, x2 - x1, y2 - y1, null);
                break;
        }
        this.repaint();
        g.dispose();
        if(function == Functions.POLYGON)
        {
            polygonPoints.remove(polygonPoints.size() - 1);
        }
    }

    private void flood(Point pt)
    {
        Color color = getMouseColors()[0];
        // addBufferedImage already called in setStartPoint()
        BufferedImage image = getLastBufferedImage();
        int w = image.getWidth();
        int h = image.getHeight();
        Color baseColor = new Color(image.getRGB(pt.x, pt.y));
        int[][] displacements = new int[][]
        {
            {  0,  1 }, 
            {  0, -1 }, 
            {  1,  0 }, 
            { -1,  0 }
        };
        LinkedList<Point> queue = new LinkedList<>();
        queue.offer(pt);
        while(!queue.isEmpty())
        {
            Point pt2 = queue.remove();
            for(int[] displacement : displacements)
            {
                int newX = pt2.x + displacement[0];
                int newY = pt2.y + displacement[1];
                if(sameColor(image, newX, newY, baseColor))
                {
                    image.setRGB(newX, newY, color.getRGB());
                    queue.offer(new Point(newX, newY));
                }
            }
        }
        this.repaint();
    }
    
    private boolean rangeValid(BufferedImage image, int x, int y)
    {
        int w = image.getWidth();
        int h = image.getHeight();
        return (x >= 0) && (x < w) && (y >= 0) && (y < h);
    }
    
    private void spraying()
    {
        double r0 = 10 * penWidth;
        int area = new Double(2 * r0 * r0 * Math.PI).intValue();
        BufferedImage image = getLastBufferedImage();
        int basicColor = getMouseColors()[0].getRGB();
        int counter = 0;
        for(int i = 0; i < area; i++)
        {
            double theta = Math.random() * 2 * Math.PI;
            double r = Math.random() * r0;
            int x = new Double(sprayerPoint.x + r * Math.cos(theta)).intValue();
            int y = new Double(sprayerPoint.y + r * Math.sin(theta)).intValue();
            if(!rangeValid(image, x, y))
            {
                continue;
            }
            if(image.getRGB(x, y) != basicColor)
            {
                image.setRGB(x, y, basicColor);
                counter++;
                if(counter == r0) // make the time for filling a disk linear to penWidth
                {
                    break;
                }
            }
        }
        this.repaint();
    }

    private boolean sameColor(BufferedImage image, int x, int y, Color baseColor)
    {
        return rangeValid(image, x, y) && (image.getRGB(x, y) == baseColor.getRGB());
    }
    
    private void drawCurve(Graphics2D g0)
    {
        Graphics2D g;
        if(g0 != null)
        {
            g = g0;
        }
        else
        {
            copyFromLastBufferedImage();
            g = getLastBufferedImage().createGraphics();
        }
        Shape shape = null;
        switch(polygonPoints.size())
        {
            case 1:
                // do nothing
                break;
            case 2:
                shape = new Line2D.Double(
                        polygonPoints.get(0).x, 
                        polygonPoints.get(0).y, 
                        polygonPoints.get(1).x, 
                        polygonPoints.get(1).y
                );
                break;
            case 3:
                shape = new QuadCurve2D.Float(
                        polygonPoints.get(0).x, 
                        polygonPoints.get(0).y, 
                        polygonPoints.get(1).x, 
                        polygonPoints.get(1).y, 
                        polygonPoints.get(2).x, 
                        polygonPoints.get(2).y
                );
                break;
            case 4:
                shape = new CubicCurve2D.Float(
                        polygonPoints.get(0).x, 
                        polygonPoints.get(0).y, 
                        polygonPoints.get(1).x, 
                        polygonPoints.get(1).y, 
                        polygonPoints.get(2).x, 
                        polygonPoints.get(2).y, 
                        polygonPoints.get(3).x, 
                        polygonPoints.get(3).y
                );
                break;
        }
        if(shape != null)
        {
            g.setColor(getMouseColors()[0]);
            g.draw(shape);
        }
        if(g0 == null)
        {
            g.dispose();
            this.repaint();
        }
    }

    public void doneSingleDrawing(Point pt)
    {
        if(function != Functions.PICK_COLOR)
        {
            saved = false;
        }
        Graphics2D g = null;
        switch(function)
        {
            case POLYGON:
                polygonPoints.add(pt);
                if(polygonPoints.get(0).distance(pt) >= 10.0)
                {
                    // painting not done yet
                    break;
                }
                Polygon polygon = new Polygon();
                for(Point pt2 : polygonPoints)
                {
                    polygon.addPoint(Util.getPointX(pt2), Util.getPointY(pt2));
                }
                bufferedImages.remove(imageIndex);
                imageIndex--;
                addBufferedImage();
                g = getLastBufferedImage().createGraphics();
                Color[] mouseColors = getMouseColors(); // [ basicColor, alternativeColor ]
                switch(fillType)
                {
                    case BORDER:
                        g.setColor(mouseColors[0]);
                        g.drawPolygon(polygon);
                        break;
                    case INTERNAL:
                        g.setColor(mouseColors[0]);
                        g.fillPolygon(polygon);
                        break;
                    case BOTH:
                        g.setColor(mouseColors[1]);
                        g.fillPolygon(polygon);
                        g.setColor(mouseColors[0]);
                        g.drawPolygon(polygon);
                        break;
                }
                g.dispose();
                this.repaint();
                polygonPoints.clear();
                break;
            case PENCIL:
            case ERASER:
                polygonPoints.clear();
                break;
            case CURVE_SELECTION:
                selectionCurve = new ArrayList<>(polygonPoints);
                selectionPolygon = new Polygon(
                        Util.getXArray(polygonPoints), 
                        Util.getYArray(polygonPoints), 
                        polygonPoints.size()
                );
                copyFromLastBufferedImage();
                g = getLastBufferedImage().createGraphics();
                g.setStroke(dottedLine);
                g.setColor(Color.BLACK);
                g.drawPolygon(selectionPolygon);
                g.dispose();
                this.repaint();
                polygonPoints.clear();
                break;
            case SPRAYER:
                sprayerTimer.cancel();
                break;
            case CURVE:
                drawCurve(null);
                if(polygonPoints.size() == 4)
                {
                    polygonPoints.clear();
                }
                break;
            case MOVING_RECTANGLE:
                selectionRect = new Rectangle();
                function = Functions.RECTANGLE_SELECTION;
                break;
            case MOVING_ARBITRARY:
                selectionCurve = null;
                selectionPolygon = null;
                function = Functions.CURVE_SELECTION;
                break;
        }
        mouseButton = MouseButton.NONE;
    }

    public void setFunction(Functions _function)
    {
        function = _function;
    }

    public Functions getFunction()
    {
        return function;
    }

    public void setLeftColor(Color color)
    {
        leftColor = color;
    }

    public void setRightColor(Color color)
    {
        rightColor = color;
    }

    public Color getLeftColor()
    {
        return leftColor;
    }

    public Color getRightColor()
    {
        return rightColor;
    }

    private Color[] getMouseColors()
    {
        Color basicColor, alternativeColor;
        switch(mouseButton)
        {
            case LEFT:
                basicColor = leftColor;
                alternativeColor = rightColor;
                break;
            case RIGHT:
                basicColor = rightColor;
                alternativeColor = leftColor;
                break;
            default:
                throw new RuntimeException("Unknown mouse button");
        }
        return new Color[] { basicColor, alternativeColor };
    }

    public void setPenWidth(int w)
    {
        penWidth = w;
    }

    public int getPenWidth()
    {
        return penWidth;
    }

    public void setFillType(FillType ft)
    {
        fillType = ft;
    }

    public BufferedImage getLastBufferedImage()
    {
        return bufferedImages.get(imageIndex);
    }

    private void addBufferedImage()
    {
        /*
         * http://stackoverflow.com/questions/3514158
         */
        BufferedImage oldImg = getLastBufferedImage();
        BufferedImage newImg = new BufferedImage(oldImg.getWidth(), oldImg.getHeight(), oldImg.getType());
        Graphics2D g = newImg.createGraphics();
        g.drawImage(oldImg, 0, 0, null);
        g.dispose();
        imageIndex++;
        bufferedImages.add(imageIndex, newImg);
    }

    private void copyFromLastBufferedImage()
    {
        BufferedImage oldImg = bufferedImages.get(imageIndex - 1);
        BufferedImage newImg = getLastBufferedImage();
        Graphics g = newImg.createGraphics();
        g.drawImage(oldImg, 0, 0, null);
        g.dispose();
    }

    public final void setImage(BufferedImage image)
    {
        bufferedImages.clear();
        BufferedImage newImage;
        if(image != null)
        {
            newImage = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        }
        else
        {
            newImage = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
        }
        Graphics g = newImage.createGraphics();
        if(image != null)
        {
            g.drawImage(image, 0, 0, null);
        }
        else
        {
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        g.dispose();
        bufferedImages.add(newImage);
        imageIndex = 0;
        this.repaint();
    }

    public void undo()
    {
        if(!polygonPoints.isEmpty())
        {
            return;
        }
        tryRemoveSelection();
        imageIndex--;
        saved = false;
        this.repaint();
    }

    public void redo()
    {
        if(!polygonPoints.isEmpty())
        {
            return;
        }
        tryRemoveSelection();
        imageIndex++;
        saved = false;
        this.repaint();
    }

    public boolean[] isUndoRedoEnabled()
    {
        boolean isUndoEnabled;
        // in the following two cases, current buffered image is not a stage of drawing
        if(hasSelection || polygonPoints.size() >= 1)
        {
            isUndoEnabled = imageIndex > 1;
        }
        else
        {
            isUndoEnabled = imageIndex > 0;
        }
        boolean isRedoEnabled = (imageIndex < bufferedImages.size() - 1);
        boolean[] results = { isUndoEnabled, isRedoEnabled };
        return results;
    }

    public void setSaved()
    {
        saved = true;
    }

    public boolean needSaving()
    {
        return !saved;
    }

    public Point getStartPoint()
    {
        return startPoint;
    }

    public void resizeImage(int w, int h)
    {
        if(w == this.getWidth() && h == this.getHeight())
        {
            return;
        }
        tryRemoveSelection();
        BufferedImage newImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = newImage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        g.drawImage(getLastBufferedImage(), 0, 0, null);
        g.dispose();
        clearBufferedImagesTail();
        bufferedImages.add(newImage);
        imageIndex = bufferedImages.size() - 1;
        // FIXME: a dirty approach
        this.setSize(w, h);
        this.setPreferredSize(new Dimension(w, h));
        this.repaint();
    }

    private void clearBufferedImagesTail()
    {
        if(imageIndex != bufferedImages.size() - 1)
        {
            // http://stackoverflow.com/questions/1184636
            bufferedImages.subList(imageIndex + 1, bufferedImages.size()).clear();
        }
    }

    public static MouseButton detectMouseButton(MouseEvent evt)
    {
        if(SwingUtilities.isLeftMouseButton(evt))
        {
            return MouseButton.LEFT;
        }
        if(SwingUtilities.isRightMouseButton(evt))
        {
            return MouseButton.RIGHT;
        }
        return MouseButton.NONE;
    }

    public void tryRemoveSelection()
    {
        if(hasSelection)
        {
            bufferedImages.remove(imageIndex);
            imageIndex--;
            setHasSelection(false);
        }
    }

    public void copySelection()
    {
        if(!hasSelection)
        {
            return;
        }
        ClipboardImage.write(getImageSelection());
    }

    public void setHasSelection(boolean flag)
    {
        hasSelection = flag;
        firePainterCanvasEvent(new PainterCanvasEvent(flag));
    }

    private BufferedImage getImageSelection()
    {
        BufferedImage lastImage = bufferedImages.get(imageIndex - 1);
        if(selectionCurve == null)
        {
            return lastImage.getSubimage(
                    selectionRect.x, selectionRect.y, 
                    selectionRect.width, selectionRect.height
            );
        }
        else
        {
            BufferedImage mask = getMaskedCurveSelection();
            Rectangle bound = selectionPolygon.getBounds();
            return mask.getSubimage(bound.x, bound.y, bound.width, bound.height);
        }
    }

    private BufferedImage getMaskedCurveSelection()
    {
        BufferedImage image = getLastBufferedImage();
        // use masking, where transparency are needed
        BufferedImage mask = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = mask.createGraphics();
        g2.setColor(new Color(0, 0, 0, 0)); // (r, g, b, a) => transparent
        g2.fillRect(0, 0, image.getWidth(), image.getHeight()); // set the whole mask transparent
        g2.setColor(Color.WHITE);
        g2.fillPolygon(selectionPolygon);
        g2.dispose();
        // calculate the mask pixel by pixel
        for(int i = 0; i < image.getWidth(); i++)
        {
            for(int j = 0; j < image.getHeight(); j++)
            {
                mask.setRGB(i, j, mask.getRGB(i, j) & image.getRGB(i, j));
            }
        }
        return mask;
    }

    public void pasteImage()
    {
        tryRemoveSelection();
        clearBufferedImagesTail();

        BufferedImage image = Util.imageToBufferedImage(ClipboardImage.read());
        addBufferedImage();
        Graphics2D g = getLastBufferedImage().createGraphics();
        g.drawImage(image, 0, 0, null);

        // simulate mouse selection
        Functions oldFunction = function;
        function = Functions.RECTANGLE_SELECTION;
        setStartPoint(new Point(0, 0), MouseButton.LEFT);
        updatePoint(new Point(image.getWidth(), image.getHeight()));
        doneSingleDrawing(null);
        function = oldFunction;
    }

    public void cutSelection(boolean toClipboard)
    {
        if(!hasSelection)
        {
            return;
        }
        tryRemoveSelection();
        clearBufferedImagesTail();
        addBufferedImage();
        Graphics2D g = getLastBufferedImage().createGraphics();
        g.setColor(Color.WHITE);
        switch(function)
        {
            case RECTANGLE_SELECTION:
                if(toClipboard)
                {
                    ClipboardImage.write(getImageSelection());
                }
                g.fillRect(selectionRect.x, selectionRect.y, selectionRect.width, selectionRect.height);
                break;
            case CURVE_SELECTION:
                if(toClipboard)
                {
                    ClipboardImage.write(getImageSelection());
                }
                g.fillPolygon(selectionPolygon);
                break;
        }
        g.dispose();
        this.repaint();
    }

    public void selectAll()
    {
        // simulate the mouse action
        Functions oldFunction = function;
        function = Functions.RECTANGLE_SELECTION;
        setStartPoint(new Point(0, 0), MouseButton.LEFT);
        updatePoint(new Point(this.getWidth(), this.getHeight()));
        doneSingleDrawing(null);
        function  = oldFunction;
    }

    public void addPainterCanvasEventListener(PainterCanvasEventListener eventListener)
    {
        painterCanvasEventListeners.add(eventListener);
    }

    private void firePainterCanvasEvent(PainterCanvasEvent evt)
    {
        for(PainterCanvasEventListener eventListener : painterCanvasEventListeners)
        {
            switch(evt.eventType)
            {
                case COLOR_CHANGED:
                    eventListener.colorChanged(evt);
                    break;
                case SELECTION_FLAG_CHANGED:
                    eventListener.selectionFlagChanged(evt);
                    break;
            }
        }
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        if(bufferedImages != null)
        {
            BufferedImage newImage = getLastBufferedImage();
            int newWidth = newImage.getWidth();
            int newHeight = newImage.getHeight();
            if(newWidth != this.getWidth() || newHeight != this.getHeight())
            {
                // FIXME: a dirty approach
                this.setPreferredSize(new Dimension(newWidth, newHeight));
                this.setSize(newWidth, newHeight);
            }
            g.drawImage(newImage, 0, 0, null);
        }
    }

    private class CanvasComponentListener implements ComponentListener
    {
        @Override
        public void componentResized(ComponentEvent e)
        {
            // Perform calculation here
            // http://stackoverflow.com/questions/1816458
            if(bufferedImages.isEmpty())
            {
                PainterCanvas canvas = PainterCanvas.this;
                canvas.setImage(null);
                canvas.repaint();
            }
        }

        // these methods are necessary because this class implements ComponentListener
        @Override
        public void componentHidden(ComponentEvent e)
        {
        }

        @Override
        public void componentMoved(ComponentEvent e)
        {
        }

        @Override
        public void componentShown(ComponentEvent e)
        {
        }
    }

    public static class PainterCanvasEvent
    {
        public enum EventType { COLOR_CHANGED, SELECTION_FLAG_CHANGED };
        private final Color newColor;
        private final MouseButton mouseButton;
        private final Boolean selectionFlag;
        private EventType eventType;

        public PainterCanvasEvent(MouseButton button, Color color)
        {
            mouseButton = button;
            newColor = color;
            selectionFlag = null;
            eventType = EventType.COLOR_CHANGED;
        }

        public PainterCanvasEvent(boolean flag)
        {
            selectionFlag = flag;
            newColor = null;
            mouseButton = null;
            eventType = EventType.SELECTION_FLAG_CHANGED;
        }

        public Color getNewColor()
        {
            return newColor;
        }

        public MouseButton getType()
        {
            return mouseButton;
        }

        public boolean getSelectionFlag()
        {
            return selectionFlag;
        }
    }

    public static class PainterCanvasEventListener
    {
        public void colorChanged(PainterCanvasEvent evt)
        {
        }

        public void selectionFlagChanged(PainterCanvasEvent evt)
        {
        }
    }
}
