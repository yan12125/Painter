/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painter.widgets;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;

/**
 *
 * @author yen
 */

/**
 * Modified from http://stackoverflow.com/questions/7834768
 */
public class ClipboardImage
{
    private static final Clipboard clipboard;
    static
    {
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public static Clipboard getClipboard()
    {
        return clipboard;
    }
    /**
     *  Retrieve an image from the system clipboard.
     *
     *  @return the image from the clipboard or null if no image is found
     */
    public static Image read()
    {
        Transferable t = clipboard.getContents( null );

        try
        {
            if (t != null && t.isDataFlavorSupported(DataFlavor.imageFlavor))
            {
                Image image = (Image)t.getTransferData(DataFlavor.imageFlavor);
                return image;
            }
        }
        catch (UnsupportedFlavorException | IOException e) {}

        return null;
    }

    /**
     * Check whether images exist in the system clipboard or not
     * 
     * @return true if image readable
     */
    public static boolean readable()
    {
        Transferable t = clipboard.getContents( null );
        return t.isDataFlavorSupported(DataFlavor.imageFlavor);
    }

    /**
     *  Place an image on the system clipboard.
     *
     *  @param  image - the image to be added to the system clipboard
     */
    public static void write(Image image)
    {
        if (image == null)
            throw new IllegalArgumentException ("Image can't be null");

        ImageTransferable transferable = new ImageTransferable( image );
        clipboard.setContents(transferable, null);
    }

    private static class ImageTransferable implements Transferable
    {
        private final Image image;

        public ImageTransferable (Image image)
        {
            this.image = image;
        }

        @Override
        public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException
        {
            if (isDataFlavorSupported(flavor))
            {
                return image;
            }
            else
            {
                throw new UnsupportedFlavorException(flavor);
            }
        }

        @Override
        public boolean isDataFlavorSupported (DataFlavor flavor)
        {
            return flavor == DataFlavor.imageFlavor;
        }

        @Override
        public DataFlavor[] getTransferDataFlavors ()
        {
            return new DataFlavor[] { DataFlavor.imageFlavor };
        }
    }
}