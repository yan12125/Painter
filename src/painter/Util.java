/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painter;

import java.awt.Font;
import java.awt.Point;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.util.ArrayList;
import static java.lang.Math.*;

/**
 *
 * @author yen
 */
public class Util {
    private Util()
    {
    }

    public static int getPointX(Point pt)
    {
        return max(1, abs(new Double(pt.getX()).intValue()));
    }

    public static int getPointY(Point pt)
    {
        return max(1, abs(new Double(pt.getY()).intValue()));
    }

    /**
     * http://stackoverflow.com/questions/13605248
     */
    public static BufferedImage imageToBufferedImage(Image image)
    {
        if (image instanceof BufferedImage)
        {
            return (BufferedImage) image;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(image, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }
    
    public static String fontString(Font font)
    {
        String[] styleNames = { "Plain", "Bold", "Italic", "BoldItalic" };
        return String.format("%s, %s, %d", font.getName(), styleNames[font.getStyle()], font.getSize());
    }
    
    public static int[] getXArray(ArrayList<Point> points)
    {
        int[] retval = new int[points.size()];
        for(int i = 0; i < points.size(); i++)
        {
            retval[i] = points.get(i).x;
        }
        return retval;
    }

    public static int[] getYArray(ArrayList<Point> points)
    {
        int[] retval = new int[points.size()];
        for(int i = 0; i < points.size(); i++)
        {
            retval[i] = points.get(i).y;
        }
        return retval;
    }
}
