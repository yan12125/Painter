/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package painter;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.datatransfer.FlavorEvent;
import java.awt.datatransfer.FlavorListener;
import java.util.*;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

import painter.widgets.ClipboardImage;

/**
 *
 * @author yen
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        filename = "";
        filepath = "";
        extension = "";
        disposed = false;
        initComponents();
        initializeImageFormats();
        updateTitle();
        this.setMinimumSize(new Dimension(640, 480));
        canvas.setParent(this);
        canvas.setFunction(PainterCanvas.Functions.LINE);
        ClipboardImage.getClipboard().addFlavorListener(new FlavorListener() {
            @Override
            public void flavorsChanged(FlavorEvent fe)
            {
                pasteMenuItem.setEnabled(ClipboardImage.readable());
            }
        });
        pasteMenuItem.setEnabled(ClipboardImage.readable());
        fillTypePanel.setVisible(false);

        functionBoxMap = new HashMap<>();
        functionBoxMap.put(PainterCanvas.Functions.LINE, lineFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.RECTANGLE_SELECTION, rectangleSelectionFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.RECTANGLE, rectangleFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.ELLIPSE, ellipseFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.POLYGON, polygonFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.ROUND_RECTANGLE, roundRectangleFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.PENCIL, pencilFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.FLOOD, floodFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.ERASER, eraserFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.PICK_COLOR, pickColorFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.SPRAYER, sprayerFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.TEXT, textFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.CURVE, curveFunctionBox);
        functionBoxMap.put(PainterCanvas.Functions.CURVE_SELECTION, curveSelectionFunctionBox);
        updateFunctionBoxesBorder();
        canvas.addPainterCanvasEventListener(new PainterCanvas.PainterCanvasEventListener(){
            @Override
            public void colorChanged(PainterCanvas.PainterCanvasEvent evt)
                {
                    switch(evt.getType())
                    {
                        case LEFT:
                            leftColorBox.setBackground(evt.getNewColor());
                            break;
                        case RIGHT:
                            rightColorBox.setBackground(evt.getNewColor());
                            break;
                    }
                }
            @Override
            public void selectionFlagChanged(PainterCanvas.PainterCanvasEvent evt)
                {
                    copyMenuItem.setEnabled(evt.getSelectionFlag());
                    cutMenuItem.setEnabled(evt.getSelectionFlag());
                    clearSelectionMenuItem.setEnabled(evt.getSelectionFlag());
                }
        });

        colorBoxes[0][0] = jPanel5;
        colorBoxes[1][0] = jPanel6;
        colorBoxes[0][1] = jPanel7;
        colorBoxes[1][1] = jPanel8;
        colorBoxes[0][2] = jPanel9;
        colorBoxes[1][2] = jPanel10;
        colorBoxes[0][3] = jPanel11;
        colorBoxes[1][3] = jPanel12;
        colorBoxes[0][4] = jPanel13;
        colorBoxes[1][4] = jPanel14;
        colorBoxes[0][5] = jPanel15;
        colorBoxes[1][5] = jPanel16;
        colorBoxes[0][6] = jPanel17;
        colorBoxes[1][6] = jPanel18;
        colorBoxes[0][7] = jPanel19;
        colorBoxes[1][7] = jPanel20;
        colorBoxes[0][8] = jPanel21;
        colorBoxes[1][8] = jPanel22;
        colorBoxes[0][9] = jPanel23;
        colorBoxes[1][9] = jPanel24;
        colorBoxes[0][10] = jPanel25;
        colorBoxes[1][10] = jPanel26;
        colorBoxes[0][11] = jPanel27;
        colorBoxes[1][11] = jPanel28;
        colorBoxes[0][12] = jPanel29;
        colorBoxes[1][12] = jPanel30;
        colorBoxes[0][13] = jPanel31;
        colorBoxes[1][13] = jPanel32;
        for(int i = 0; i < 2; i++)
        {
            for(int j = 0; j < 14; j++)
            {
                colorBoxes[i][j].setBackground(colors[i][j]);
                colorBoxes[i][j].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseReleased(MouseEvent e)
                    {
                        colorBoxClicked(e);
                    }
                });
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        statusBar = new javax.swing.JPanel();
        currentColorsPanel = new javax.swing.JPanel();
        leftColorBox = new javax.swing.JPanel();
        rightColorBox = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jPanel22 = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jPanel27 = new javax.swing.JPanel();
        jPanel28 = new javax.swing.JPanel();
        jPanel29 = new javax.swing.JPanel();
        jPanel30 = new javax.swing.JPanel();
        jPanel31 = new javax.swing.JPanel();
        jPanel32 = new javax.swing.JPanel();
        functionsBoxesPanel = new javax.swing.JPanel();
        lineFunctionBox = new javax.swing.JLabel();
        eraserFunctionBox = new javax.swing.JLabel();
        pencilFunctionBox = new javax.swing.JLabel();
        sprayerFunctionBox = new javax.swing.JLabel();
        rectangleFunctionBox = new javax.swing.JLabel();
        ellipseFunctionBox = new javax.swing.JLabel();
        rectangleSelectionFunctionBox = new javax.swing.JLabel();
        textFunctionBox = new javax.swing.JLabel();
        curveFunctionBox = new javax.swing.JLabel();
        polygonFunctionBox = new javax.swing.JLabel();
        roundRectangleFunctionBox = new javax.swing.JLabel();
        curveSelectionFunctionBox = new javax.swing.JLabel();
        pickColorFunctionBox = new javax.swing.JLabel();
        lineFunctionBox4 = new javax.swing.JLabel();
        lineFunctionBox5 = new javax.swing.JLabel();
        floodFunctionBox = new javax.swing.JLabel();
        coordinationPanel = new javax.swing.JPanel();
        coordinationLabel = new javax.swing.JLabel();
        sizePanel = new javax.swing.JPanel();
        sizeLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel49 = new javax.swing.JPanel();
        canvas = new painter.PainterCanvas();
        fillTypePanel = new javax.swing.JPanel();
        borderPanelContainer = new javax.swing.JPanel();
        borderPanel = new javax.swing.JPanel();
        bothFillPanelContainer = new javax.swing.JPanel();
        bothFillPanel = new javax.swing.JPanel();
        internalFillPanelContainer = new javax.swing.JPanel();
        internalFillPanel = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newFileMenuItem = new javax.swing.JMenuItem();
        openFileMenuItem = new javax.swing.JMenuItem();
        saveFileMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        undoMenuItem = new javax.swing.JMenuItem();
        redoMenuItem = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        cutMenuItem = new javax.swing.JMenuItem();
        copyMenuItem = new javax.swing.JMenuItem();
        pasteMenuItem = new javax.swing.JMenuItem();
        clearSelectionMenuItem = new javax.swing.JMenuItem();
        selectAllMenuItem = new javax.swing.JMenuItem();
        imageMenu = new javax.swing.JMenu();
        attributesMenuItem = new javax.swing.JMenuItem();
        drawingMenu = new javax.swing.JMenu();
        changeLeftColorMenuItem = new javax.swing.JMenuItem();
        changeRightColorMenuItem = new javax.swing.JMenuItem();
        penWidthMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        statusBar.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout statusBarLayout = new javax.swing.GroupLayout(statusBar);
        statusBar.setLayout(statusBarLayout);
        statusBarLayout.setHorizontalGroup(
            statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        statusBarLayout.setVerticalGroup(
            statusBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 24, Short.MAX_VALUE)
        );

        currentColorsPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        leftColorBox.setBackground(java.awt.Color.black);
        leftColorBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout leftColorBoxLayout = new javax.swing.GroupLayout(leftColorBox);
        leftColorBox.setLayout(leftColorBoxLayout);
        leftColorBoxLayout.setHorizontalGroup(
            leftColorBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        leftColorBoxLayout.setVerticalGroup(
            leftColorBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 19, Short.MAX_VALUE)
        );

        rightColorBox.setBackground(java.awt.Color.white);
        rightColorBox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout rightColorBoxLayout = new javax.swing.GroupLayout(rightColorBox);
        rightColorBox.setLayout(rightColorBoxLayout);
        rightColorBoxLayout.setHorizontalGroup(
            rightColorBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 18, Short.MAX_VALUE)
        );
        rightColorBoxLayout.setVerticalGroup(
            rightColorBoxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 17, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout currentColorsPanelLayout = new javax.swing.GroupLayout(currentColorsPanel);
        currentColorsPanel.setLayout(currentColorsPanelLayout);
        currentColorsPanelLayout.setHorizontalGroup(
            currentColorsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(currentColorsPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(rightColorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(currentColorsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(leftColorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        currentColorsPanelLayout.setVerticalGroup(
            currentColorsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(currentColorsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(leftColorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(rightColorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel15.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel16.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel17.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel18.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel19.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel20.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel21.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel23.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel24.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel25.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel26.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel27.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel28.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel29.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel30.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel31.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        jPanel32.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel21, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel22, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lineFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool1.gif"))); // NOI18N
        lineFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lineFunctionBoxMouseClicked(evt);
            }
        });

        eraserFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool6.gif"))); // NOI18N
        eraserFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                eraserFunctionBoxMouseClicked(evt);
            }
        });

        pencilFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool7.gif"))); // NOI18N
        pencilFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pencilFunctionBoxMouseClicked(evt);
            }
        });

        sprayerFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool16.gif"))); // NOI18N
        sprayerFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sprayerFunctionBoxMouseClicked(evt);
            }
        });

        rectangleFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool2.gif"))); // NOI18N
        rectangleFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rectangleFunctionBoxMouseClicked(evt);
            }
        });

        ellipseFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool3.gif"))); // NOI18N
        ellipseFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ellipseFunctionBoxMouseClicked(evt);
            }
        });

        rectangleSelectionFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool11.gif"))); // NOI18N
        rectangleSelectionFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rectangleSelectionFunctionBoxMouseClicked(evt);
            }
        });

        textFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool10.gif"))); // NOI18N
        textFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textFunctionBoxMouseClicked(evt);
            }
        });

        curveFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool5.gif"))); // NOI18N
        curveFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                curveFunctionBoxMouseClicked(evt);
            }
        });

        polygonFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool9.gif"))); // NOI18N
        polygonFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                polygonFunctionBoxMouseClicked(evt);
            }
        });

        roundRectangleFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool4.gif"))); // NOI18N
        roundRectangleFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                roundRectangleFunctionBoxMouseClicked(evt);
            }
        });

        curveSelectionFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool17.gif"))); // NOI18N
        curveSelectionFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                curveSelectionFunctionBoxMouseClicked(evt);
            }
        });

        pickColorFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool18.gif"))); // NOI18N
        pickColorFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pickColorFunctionBoxMouseClicked(evt);
            }
        });

        lineFunctionBox4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool13.gif"))); // NOI18N
        lineFunctionBox4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lineFunctionBox4MouseClicked(evt);
            }
        });

        lineFunctionBox5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool19.gif"))); // NOI18N
        lineFunctionBox5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lineFunctionBox5MouseClicked(evt);
            }
        });

        floodFunctionBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/painter/resources/tool15.gif"))); // NOI18N
        floodFunctionBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                floodFunctionBoxMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout functionsBoxesPanelLayout = new javax.swing.GroupLayout(functionsBoxesPanel);
        functionsBoxesPanel.setLayout(functionsBoxesPanelLayout);
        functionsBoxesPanelLayout.setHorizontalGroup(
            functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                        .addComponent(eraserFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(floodFunctionBox))
                    .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                        .addComponent(curveSelectionFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rectangleSelectionFunctionBox))
                    .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                        .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(ellipseFunctionBox)
                            .addComponent(lineFunctionBox)
                            .addComponent(pencilFunctionBox)
                            .addComponent(sprayerFunctionBox)
                            .addComponent(rectangleFunctionBox)
                            .addComponent(pickColorFunctionBox))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(roundRectangleFunctionBox)
                            .addComponent(polygonFunctionBox)
                            .addComponent(textFunctionBox)
                            .addComponent(curveFunctionBox)
                            .addComponent(lineFunctionBox4)
                            .addComponent(lineFunctionBox5))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        functionsBoxesPanelLayout.setVerticalGroup(
            functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rectangleSelectionFunctionBox)
                    .addComponent(curveSelectionFunctionBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(eraserFunctionBox)
                    .addComponent(floodFunctionBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pickColorFunctionBox)
                    .addComponent(lineFunctionBox4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pencilFunctionBox)
                    .addComponent(lineFunctionBox5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                        .addComponent(textFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(curveFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(polygonFunctionBox))
                    .addGroup(functionsBoxesPanelLayout.createSequentialGroup()
                        .addComponent(sprayerFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lineFunctionBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rectangleFunctionBox)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(functionsBoxesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ellipseFunctionBox)
                    .addComponent(roundRectangleFunctionBox))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        coordinationPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout coordinationPanelLayout = new javax.swing.GroupLayout(coordinationPanel);
        coordinationPanel.setLayout(coordinationPanelLayout);
        coordinationPanelLayout.setHorizontalGroup(
            coordinationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(coordinationLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 113, Short.MAX_VALUE)
        );
        coordinationPanelLayout.setVerticalGroup(
            coordinationPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, coordinationPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(coordinationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        sizePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        javax.swing.GroupLayout sizePanelLayout = new javax.swing.GroupLayout(sizePanel);
        sizePanel.setLayout(sizePanelLayout);
        sizePanelLayout.setHorizontalGroup(
            sizePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sizeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
        );
        sizePanelLayout.setVerticalGroup(
            sizePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(sizeLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel49.setBackground(java.awt.Color.gray);

        canvas.setBackground(java.awt.Color.white);
        canvas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                canvasMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                canvasMouseReleased(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                canvasMouseExited(evt);
            }
        });
        canvas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                canvasMouseMoved(evt);
            }
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                canvasMouseDragged(evt);
            }
        });

        javax.swing.GroupLayout canvasLayout = new javax.swing.GroupLayout(canvas);
        canvas.setLayout(canvasLayout);
        canvasLayout.setHorizontalGroup(
            canvasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 512, Short.MAX_VALUE)
        );
        canvasLayout.setVerticalGroup(
            canvasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 384, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel49Layout = new javax.swing.GroupLayout(jPanel49);
        jPanel49.setLayout(jPanel49Layout);
        jPanel49Layout.setHorizontalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(canvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel49Layout.setVerticalGroup(
            jPanel49Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel49Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(canvas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane1.setViewportView(jPanel49);

        fillTypePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        borderPanelContainer.setBackground(new java.awt.Color(51, 102, 255));

        borderPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        borderPanel.setPreferredSize(new java.awt.Dimension(30, 15));
        borderPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                borderPanelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout borderPanelLayout = new javax.swing.GroupLayout(borderPanel);
        borderPanel.setLayout(borderPanelLayout);
        borderPanelLayout.setHorizontalGroup(
            borderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        borderPanelLayout.setVerticalGroup(
            borderPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 13, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout borderPanelContainerLayout = new javax.swing.GroupLayout(borderPanelContainer);
        borderPanelContainer.setLayout(borderPanelContainerLayout);
        borderPanelContainerLayout.setHorizontalGroup(
            borderPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, borderPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(borderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        borderPanelContainerLayout.setVerticalGroup(
            borderPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, borderPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(borderPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        bothFillPanel.setBackground(java.awt.Color.lightGray);
        bothFillPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        bothFillPanel.setPreferredSize(new java.awt.Dimension(30, 15));
        bothFillPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bothFillPanelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout bothFillPanelLayout = new javax.swing.GroupLayout(bothFillPanel);
        bothFillPanel.setLayout(bothFillPanelLayout);
        bothFillPanelLayout.setHorizontalGroup(
            bothFillPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 28, Short.MAX_VALUE)
        );
        bothFillPanelLayout.setVerticalGroup(
            bothFillPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 13, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout bothFillPanelContainerLayout = new javax.swing.GroupLayout(bothFillPanelContainer);
        bothFillPanelContainer.setLayout(bothFillPanelContainerLayout);
        bothFillPanelContainerLayout.setHorizontalGroup(
            bothFillPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(bothFillPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(bothFillPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        bothFillPanelContainerLayout.setVerticalGroup(
            bothFillPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, bothFillPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(bothFillPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        internalFillPanel.setBackground(java.awt.Color.lightGray);
        internalFillPanel.setPreferredSize(new java.awt.Dimension(30, 15));
        internalFillPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                internalFillPanelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout internalFillPanelLayout = new javax.swing.GroupLayout(internalFillPanel);
        internalFillPanel.setLayout(internalFillPanelLayout);
        internalFillPanelLayout.setHorizontalGroup(
            internalFillPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );
        internalFillPanelLayout.setVerticalGroup(
            internalFillPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 15, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout internalFillPanelContainerLayout = new javax.swing.GroupLayout(internalFillPanelContainer);
        internalFillPanelContainer.setLayout(internalFillPanelContainerLayout);
        internalFillPanelContainerLayout.setHorizontalGroup(
            internalFillPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, internalFillPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(internalFillPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        internalFillPanelContainerLayout.setVerticalGroup(
            internalFillPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(internalFillPanelContainerLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(internalFillPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );

        javax.swing.GroupLayout fillTypePanelLayout = new javax.swing.GroupLayout(fillTypePanel);
        fillTypePanel.setLayout(fillTypePanelLayout);
        fillTypePanelLayout.setHorizontalGroup(
            fillTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(fillTypePanelLayout.createSequentialGroup()
                .addGroup(fillTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(borderPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bothFillPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(internalFillPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        fillTypePanelLayout.setVerticalGroup(
            fillTypePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, fillTypePanelLayout.createSequentialGroup()
                .addComponent(borderPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(bothFillPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(internalFillPanelContainer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        fileMenu.setMnemonic('F');
        fileMenu.setText("File");

        newFileMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newFileMenuItem.setMnemonic('N');
        newFileMenuItem.setText("New file");
        newFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newFileMenuItem);

        openFileMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        openFileMenuItem.setMnemonic('O');
        openFileMenuItem.setText("Open File");
        openFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openFileMenuItem);

        saveFileMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveFileMenuItem.setMnemonic('S');
        saveFileMenuItem.setText("Save File");
        saveFileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveFileMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveFileMenuItem);

        saveAsMenuItem.setMnemonic('A');
        saveAsMenuItem.setText("Save As");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);
        fileMenu.add(jSeparator1);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        jMenuBar1.add(fileMenu);

        editMenu.setMnemonic('E');
        editMenu.setText("Edit");

        undoMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        undoMenuItem.setMnemonic('U');
        undoMenuItem.setText("Undo");
        undoMenuItem.setEnabled(false);
        undoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(undoMenuItem);

        redoMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        redoMenuItem.setMnemonic('R');
        redoMenuItem.setText("Redo");
        redoMenuItem.setEnabled(false);
        redoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redoMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(redoMenuItem);
        editMenu.add(jSeparator2);

        cutMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        cutMenuItem.setText("Cut");
        cutMenuItem.setEnabled(false);
        cutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(cutMenuItem);

        copyMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        copyMenuItem.setMnemonic('C');
        copyMenuItem.setText("Copy");
        copyMenuItem.setEnabled(false);
        copyMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(copyMenuItem);

        pasteMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        pasteMenuItem.setMnemonic('P');
        pasteMenuItem.setText("Paste");
        pasteMenuItem.setEnabled(false);
        pasteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(pasteMenuItem);

        clearSelectionMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
        clearSelectionMenuItem.setText("Clear Selection");
        clearSelectionMenuItem.setEnabled(false);
        clearSelectionMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearSelectionMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(clearSelectionMenuItem);

        selectAllMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        selectAllMenuItem.setText("Select All");
        selectAllMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(selectAllMenuItem);

        jMenuBar1.add(editMenu);

        imageMenu.setMnemonic('I');
        imageMenu.setText("Image");

        attributesMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        attributesMenuItem.setMnemonic('A');
        attributesMenuItem.setText("Attributes...");
        attributesMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                attributesMenuItemActionPerformed(evt);
            }
        });
        imageMenu.add(attributesMenuItem);

        jMenuBar1.add(imageMenu);

        drawingMenu.setMnemonic('D');
        drawingMenu.setText("Drawing");

        changeLeftColorMenuItem.setText("Change Left Color...");
        changeLeftColorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeLeftColorMenuItemActionPerformed(evt);
            }
        });
        drawingMenu.add(changeLeftColorMenuItem);

        changeRightColorMenuItem.setText("Change Right Color...");
        changeRightColorMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeRightColorMenuItemActionPerformed(evt);
            }
        });
        drawingMenu.add(changeRightColorMenuItem);

        penWidthMenuItem.setText("Change Pen Width...");
        penWidthMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                penWidthMenuItemActionPerformed(evt);
            }
        });
        drawingMenu.add(penWidthMenuItem);

        jMenuBar1.add(drawingMenu);

        helpMenu.setMnemonic('H');
        helpMenu.setText("Help");

        aboutMenuItem.setMnemonic('A');
        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(statusBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(coordinationPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sizePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(currentColorsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(functionsBoxesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fillTypePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(functionsBoxesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fillTypePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(currentColorsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(statusBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(coordinationPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sizePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        // TODO add your handling code here:
        checkExiting();
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void openFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openFileMenuItemActionPerformed
        // TODO add your handling code here:
        if(checkSaving() == Answer.CANCEL)
        {
            return;
        }
        int retval = fc.showOpenDialog(this);
        if(retval == JFileChooser.APPROVE_OPTION)
        {
            try
            {
                File source = fc.getSelectedFile();
                canvas.setImage(ImageIO.read(source));
                filename = source.getName();
                filepath = source.getAbsolutePath();
                extension = ((FileNameExtensionFilter)fc.getFileFilter()).getExtensions()[0];
                updateUndoRedo();
            }
            catch(IOException e)
            {
                JOptionPane.showMessageDialog(null, "Failed to read file", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_openFileMenuItemActionPerformed

    private void canvasMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canvasMousePressed
        // TODO add your handling code here:
        canvas.setStartPoint(evt.getPoint(), PainterCanvas.detectMouseButton(evt));
        updateUndoRedo();
    }//GEN-LAST:event_canvasMousePressed

    private void canvasMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canvasMouseDragged
        // TODO add your handling code here:
        Point endPoint = evt.getPoint();
        Point startPoint = canvas.getStartPoint();
        canvas.updatePoint(endPoint);
        int[] dimensions = new int[] { endPoint.x - startPoint.x, endPoint.y - startPoint.y };
        sizeLabel.setText(String.format("%dx%d", dimensions[0], dimensions[1]));
    }//GEN-LAST:event_canvasMouseDragged

    private void canvasMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canvasMouseReleased
        // TODO add your handling code here:
        canvas.doneSingleDrawing(evt.getPoint());
        updateUndoRedo();
        sizeLabel.setText("");
    }//GEN-LAST:event_canvasMouseReleased

    private void undoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoMenuItemActionPerformed
        // TODO add your handling code here:
        canvas.undo();
        updateUndoRedo();
    }//GEN-LAST:event_undoMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Simple Painter\nBy Yen Chi Hsuan", "About", JOptionPane.DEFAULT_OPTION);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void redoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redoMenuItemActionPerformed
        // TODO add your handling code here:
        canvas.redo();
        updateUndoRedo();
    }//GEN-LAST:event_redoMenuItemActionPerformed

    private void saveFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveFileMenuItemActionPerformed
        // TODO add your handling code here:
        saveToFile(false);
    }//GEN-LAST:event_saveFileMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        // TODO add your handling code here:
        saveToFile(true);
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void newFileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newFileMenuItemActionPerformed
        // TODO add your handling code here:
        if(checkSaving() != Answer.CANCEL)
        {
            canvas.setImage(null);
            updateUndoRedo();
        }
    }//GEN-LAST:event_newFileMenuItemActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        checkExiting();
    }//GEN-LAST:event_formWindowClosing

    private void lineFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lineFunctionBoxMouseClicked
        // TODO add your handling code here:
        canvas.setFunction(PainterCanvas.Functions.LINE);
    }//GEN-LAST:event_lineFunctionBoxMouseClicked

    private void attributesMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_attributesMenuItemActionPerformed
        // TODO add your handling code here:
        AttributesDialog dialog = new AttributesDialog(this, true);
        dialog.setDimension(canvas.getWidth(), canvas.getHeight());
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        if(dialog.isOk())
        {
            int[] dimensions = dialog.getDimensions();
            canvas.resizeImage(dimensions[0], dimensions[1]);
            updateUndoRedo();
        }
        dialog.dispose();
    }//GEN-LAST:event_attributesMenuItemActionPerformed

    private void canvasMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canvasMouseMoved
        // TODO add your handling code here:
        coordinationLabel.setText(String.format("%d,%d", evt.getX(), evt.getY()));
    }//GEN-LAST:event_canvasMouseMoved

    private void canvasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canvasMouseExited
        // TODO add your handling code here:
        coordinationLabel.setText("");
    }//GEN-LAST:event_canvasMouseExited

    private void rectangleFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rectangleFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.RECTANGLE);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_rectangleFunctionBoxMouseClicked

    private void ellipseFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ellipseFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.ELLIPSE);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_ellipseFunctionBoxMouseClicked

    private void rectangleSelectionFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rectangleSelectionFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.RECTANGLE_SELECTION);
    }//GEN-LAST:event_rectangleSelectionFunctionBoxMouseClicked

    private void textFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.TEXT);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_textFunctionBoxMouseClicked

    private void curveFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_curveFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.CURVE);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_curveFunctionBoxMouseClicked

    private void polygonFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_polygonFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.POLYGON);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_polygonFunctionBoxMouseClicked

    private void roundRectangleFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_roundRectangleFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.ROUND_RECTANGLE);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_roundRectangleFunctionBoxMouseClicked

    private void curveSelectionFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_curveSelectionFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.CURVE_SELECTION);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_curveSelectionFunctionBoxMouseClicked

    private void pickColorFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pickColorFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.PICK_COLOR);
    }//GEN-LAST:event_pickColorFunctionBoxMouseClicked

    private void lineFunctionBox4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lineFunctionBox4MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lineFunctionBox4MouseClicked

    private void lineFunctionBox5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lineFunctionBox5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_lineFunctionBox5MouseClicked

    private void floodFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_floodFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.FLOOD);
    }//GEN-LAST:event_floodFunctionBoxMouseClicked

    private void copyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyMenuItemActionPerformed
        canvas.copySelection();
    }//GEN-LAST:event_copyMenuItemActionPerformed

    private void pasteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteMenuItemActionPerformed
        canvas.pasteImage();
    }//GEN-LAST:event_pasteMenuItemActionPerformed

    private void cutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutMenuItemActionPerformed
        canvas.cutSelection(true);
    }//GEN-LAST:event_cutMenuItemActionPerformed

    private void selectAllMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectAllMenuItemActionPerformed
        canvas.selectAll();
        canvas.setFunction(PainterCanvas.Functions.RECTANGLE_SELECTION);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_selectAllMenuItemActionPerformed

    private void clearSelectionMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearSelectionMenuItemActionPerformed
        canvas.cutSelection(false);
    }//GEN-LAST:event_clearSelectionMenuItemActionPerformed

    private void changeLeftColorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeLeftColorMenuItemActionPerformed
        setLeftColor(JColorChooser.showDialog(this, "Choose a Color", canvas.getLeftColor()));
    }//GEN-LAST:event_changeLeftColorMenuItemActionPerformed

    private void changeRightColorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeRightColorMenuItemActionPerformed
        setRightColor(JColorChooser.showDialog(this, "Choose a Color", canvas.getRightColor()));
    }//GEN-LAST:event_changeRightColorMenuItemActionPerformed

    private void penWidthMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_penWidthMenuItemActionPerformed
        while(true)
        {
            PenWidthDialog dialog = new PenWidthDialog(this, true);
            dialog.setPenWidth(canvas.getPenWidth());
            dialog.setVisible(true);
            if(dialog.isCancelled())
            {
                break;
            }
            int w = dialog.getPenWidth();
            if(w >= 1)
            {
                canvas.setPenWidth(w);
                dialog.dispose();
                break;
            }
            dialog.dispose();
        }
    }//GEN-LAST:event_penWidthMenuItemActionPerformed

    private void borderPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_borderPanelMouseClicked
        updateFillTypeSelection(PainterCanvas.FillType.BORDER);
    }//GEN-LAST:event_borderPanelMouseClicked

    private void bothFillPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bothFillPanelMouseClicked
        updateFillTypeSelection(PainterCanvas.FillType.BOTH);
    }//GEN-LAST:event_bothFillPanelMouseClicked

    private void internalFillPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_internalFillPanelMouseClicked
        updateFillTypeSelection(PainterCanvas.FillType.INTERNAL);
    }//GEN-LAST:event_internalFillPanelMouseClicked

    private void pencilFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pencilFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.PENCIL);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_pencilFunctionBoxMouseClicked

    private void eraserFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eraserFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.ERASER);
    }//GEN-LAST:event_eraserFunctionBoxMouseClicked

    private void sprayerFunctionBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sprayerFunctionBoxMouseClicked
        canvas.setFunction(PainterCanvas.Functions.SPRAYER);
        updateFunctionBoxesBorder();
    }//GEN-LAST:event_sprayerFunctionBoxMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem attributesMenuItem;
    private javax.swing.JPanel borderPanel;
    private javax.swing.JPanel borderPanelContainer;
    private javax.swing.JPanel bothFillPanel;
    private javax.swing.JPanel bothFillPanelContainer;
    private painter.PainterCanvas canvas;
    private javax.swing.JMenuItem changeLeftColorMenuItem;
    private javax.swing.JMenuItem changeRightColorMenuItem;
    private javax.swing.JMenuItem clearSelectionMenuItem;
    private javax.swing.JLabel coordinationLabel;
    private javax.swing.JPanel coordinationPanel;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JPanel currentColorsPanel;
    private javax.swing.JLabel curveFunctionBox;
    private javax.swing.JLabel curveSelectionFunctionBox;
    private javax.swing.JMenuItem cutMenuItem;
    private javax.swing.JMenu drawingMenu;
    private javax.swing.JMenu editMenu;
    private javax.swing.JLabel ellipseFunctionBox;
    private javax.swing.JLabel eraserFunctionBox;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel fillTypePanel;
    private javax.swing.JLabel floodFunctionBox;
    private javax.swing.JPanel functionsBoxesPanel;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenu imageMenu;
    private javax.swing.JPanel internalFillPanel;
    private javax.swing.JPanel internalFillPanelContainer;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel49;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPanel leftColorBox;
    private javax.swing.JLabel lineFunctionBox;
    private javax.swing.JLabel lineFunctionBox4;
    private javax.swing.JLabel lineFunctionBox5;
    private javax.swing.JMenuItem newFileMenuItem;
    private javax.swing.JMenuItem openFileMenuItem;
    private javax.swing.JMenuItem pasteMenuItem;
    private javax.swing.JMenuItem penWidthMenuItem;
    private javax.swing.JLabel pencilFunctionBox;
    private javax.swing.JLabel pickColorFunctionBox;
    private javax.swing.JLabel polygonFunctionBox;
    private javax.swing.JLabel rectangleFunctionBox;
    private javax.swing.JLabel rectangleSelectionFunctionBox;
    private javax.swing.JMenuItem redoMenuItem;
    private javax.swing.JPanel rightColorBox;
    private javax.swing.JLabel roundRectangleFunctionBox;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveFileMenuItem;
    private javax.swing.JMenuItem selectAllMenuItem;
    private javax.swing.JLabel sizeLabel;
    private javax.swing.JPanel sizePanel;
    private javax.swing.JLabel sprayerFunctionBox;
    private javax.swing.JPanel statusBar;
    private javax.swing.JLabel textFunctionBox;
    private javax.swing.JMenuItem undoMenuItem;
    // End of variables declaration//GEN-END:variables

    private enum Answer { YES, NO, CANCEL };
    private final JPanel[][] colorBoxes = new JPanel[2][14];
    private JFileChooser fc;
    private String filename;
    private String filepath;
    private String extension; // in fact, format
    private boolean disposed; // to prevent repeated call of checkExiting()
    private final static String imageTypesFactory[][] = {
        { "bmp", "Windows bitmap" }, 
        { "gif", "Graphics Interchange Format" }, 
        { "jpg,jpeg", "Joint Photographic Experts Group" }, 
        { "png", "Portable Network Graphics" }, 
        { "wbmp", "Wireless Application Protocol Bitmap Format" }
    };
    private HashMap<PainterCanvas.Functions, JLabel> functionBoxMap;
    private final static Color[][] colors = {
        {
            new Color(0  ,0  ,0), 
            new Color(128,128,128), 
            new Color(128,0  ,0), 
            new Color(128,128,0), 
            new Color(0  ,100,0), 
            new Color(0  ,128,128), 
            new Color(0  ,0  ,128), 
            new Color(128,0  ,128), 
            new Color(207,181,59), 
            new Color(111,128,144), 
            new Color(75 ,106,148), 
            new Color(25 ,25 ,112), 
            new Color(138,43 ,226), 
            new Color(165,42 ,42), 
        }, 
        {
            new Color(255,255,255), 
            new Color(192,192,192), 
            new Color(255,0  ,0), 
            new Color(255,255,0), 
            new Color(0  ,255,0), 
            new Color(0  ,255,255), 
            new Color(0  ,0  ,255), 
            new Color(255,0  ,255), 
            new Color(255,244,79), 
            new Color(76 ,187,23), 
            new Color(127,255,212), 
            new Color(204,204,255), 
            new Color(255,127,80), 
            new Color(255,117,24), 
        }
    };

    private void colorBoxClicked(MouseEvent e)
    {
        JPanel selectedPanel = (JPanel)e.getSource();
        int row = -1, col = -1;
        for(int i = 0; i < 2; i++)
        {
            for(int j = 0; j < 14; j++)
            {
                if(colorBoxes[i][j] == selectedPanel)
                {
                    row = i;
                    col = j;
                }
            }
        }
        if(SwingUtilities.isLeftMouseButton(e))
        {
            setLeftColor(colors[row][col]);
        }
        if(SwingUtilities.isRightMouseButton(e))
        {
            setRightColor(colors[row][col]);
        }
    }

    private void setLeftColor(Color color)
    {
        leftColorBox.setBackground(color);
        canvas.setLeftColor(color);
    }
    private void setRightColor(Color color)
    {
        rightColorBox.setBackground(color);
        canvas.setRightColor(color);
    }

    private void updateUndoRedo()
    {
        boolean[] flags = canvas.isUndoRedoEnabled();
        undoMenuItem.setEnabled(flags[0]);
        redoMenuItem.setEnabled(flags[1]);
        updateTitle();
    }

    private void initializeImageFormats()
    {
        // java.awt has List, too
        java.util.List<String> formats = Arrays.asList(ImageIO.getWriterFormatNames());
        fc = new JFileChooser();
        fc.setAcceptAllFileFilterUsed(false);
        for(String[] imageType : imageTypesFactory)
        {
            String[] extensionsInternal = imageType[0].split(",");
            int index = formats.indexOf(extensionsInternal[0]);
            if(index != -1)
            {
                String description = imageType[1];
                for(String extensionInternal : extensionsInternal)
                {
                    description += String.format(" (*.%s)", extensionInternal);
                }
                // http://blog.xuite.net/ax040807/LetTheGirlGo/58422050-JFileChooser+chooser+%E5%AF%A6%E4%BD%9C
                fc.addChoosableFileFilter(new FileNameExtensionFilter(description, extensionsInternal));
            }
        }
    }

    private Answer saveToFile(boolean saveAs)
    {
        File target;
        if(saveAs || filename.equals("") || !(new File(filepath)).exists())
        {
            boolean isReplace = false;
            while(true)
            {
                int retval = fc.showSaveDialog(this);
                if(retval != JFileChooser.APPROVE_OPTION)
                {
                    return Answer.CANCEL;
                }
                if(fc.getSelectedFile().exists())
                {
                    Object[] options = { "Yes", "No" };
                    int answer = JOptionPane.showOptionDialog(null, "File exists. Overwrite?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
                    if(answer == JOptionPane.YES_OPTION)
                    {
                        isReplace = true;
                        break;
                    }
                    // continue if NO chosen
                }
                else
                {
                    break;
                }
            }
            target = fc.getSelectedFile();
            filename = target.getName();
            filepath = target.getAbsolutePath();
            extension = ((FileNameExtensionFilter)fc.getFileFilter()).getExtensions()[0];
            if(!isReplace && filename.indexOf(".") == -1)
            {
                String extensionPart = "." + extension;
                filename += extensionPart;
                filepath += extensionPart;
                target = new File(filepath);
            }
        }
        else
        {
            target = new File(filepath);
        }
        // Writing files
        try
        {
            ImageIO.write(canvas.getLastBufferedImage(), extension, target);
            canvas.setSaved(); // this should be before updateUndoRedo()
            updateUndoRedo();
        }
        catch(IOException e)
        {
            JOptionPane.showMessageDialog(null, "Write to the file failed!", "Error!", JOptionPane.ERROR_MESSAGE);
        }
        return Answer.YES;
    }

    private void updateTitle()
    {
        String filename_ = ("".equals(filename))?"Untitled":filename;
        String extension_ = ("".equals(extension))?"Unknown format":extension.toUpperCase();
        String title = String.format("Painter - %s [%s]", filename_, extension_);
        if(canvas.needSaving())
        {
            title += " [modified]";
        }
        this.setTitle(title);
    }

    private Answer checkSaving()
    {
        if(canvas.needSaving())
        {
            int answer = JOptionPane.showConfirmDialog(null, "Image modified. Saving?", "Question", JOptionPane.YES_NO_CANCEL_OPTION);
            if(answer == JOptionPane.YES_OPTION)
            {
                return saveToFile(false);
            }
            else if(answer == JOptionPane.NO_OPTION)
            {
                return Answer.NO;
            }
            else
            {
                return Answer.CANCEL;
            }
        }
        else
        {
            return Answer.YES;
        }
    }

    private void checkExiting()
    {
        if(!disposed && checkSaving() != Answer.CANCEL)
        {
            disposed = true;
            this.dispose();
        }
    }

    private void updateFunctionBoxesBorder()
    {
        Border border = BorderFactory.createLoweredBevelBorder();
        // use a transparent border to prevent layout change of function boxes 
        // when some box clicked
        // 3 is a empirical value; the minimum valid value on Linux/Metal is 2
        Border transparentBorder = BorderFactory.createLineBorder(new Color(0, 0, 0, 0), 3);

        Component[] components = functionsBoxesPanel.getComponents();
        for(Component component : components)
        {
            JLabel functionBox = (JLabel)component;
            functionBox.setBorder(transparentBorder);
            boolean listenerAdded = false;
            for(MouseListener ml : functionBox.getMouseListeners())
            {
                if(ml instanceof FunctionBoxListener)
                {
                    listenerAdded = true;
                }
            }
            if(!listenerAdded)
            {
                functionBox.addMouseListener(new FunctionBoxListener());
            }
        }

        switch(canvas.getFunction())
        {
            case RECTANGLE:
            case ELLIPSE:
            case POLYGON:
            case ROUND_RECTANGLE:
                fillTypePanel.setVisible(true);
                break;
            default:
                fillTypePanel.setVisible(false);
                break;
        }
        if(functionBoxMap.containsKey(canvas.getFunction()))
        {
            functionBoxMap.get(canvas.getFunction()).setBorder(border);
        }
    }

    private void updateFillTypeSelection(PainterCanvas.FillType fillType)
    {
        Color defaultColor = fillTypePanel.getBackground();
        Color newColor = new Color(51, 102, 255);
        borderPanelContainer.setBackground(fillType == PainterCanvas.FillType.BORDER ? newColor: defaultColor);
        bothFillPanelContainer.setBackground(fillType == PainterCanvas.FillType.BOTH ? newColor: defaultColor);
        internalFillPanelContainer.setBackground(fillType == PainterCanvas.FillType.INTERNAL ? newColor: defaultColor);
        canvas.setFillType(fillType);
    }

    private class FunctionBoxListener extends MouseAdapter
    {
        @Override
        public void mouseClicked(MouseEvent me)
        {
            updateFunctionBoxesBorder();
            canvas.tryRemoveSelection();
        }
    }
}
